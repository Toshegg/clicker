namespace :clicker do
  desc 'Generate schedules for next 2 month'

  task generate_schedules: :environment do
    if Schedule.all.empty?
      bet_accepts_at = DateTime.now
    else
      bet_accepts_at = Schedule.last.game_ends_at
    end
    counter = bet_accepts_at

    while counter < bet_accepts_at + 2.month
      Schedule.create(bets_accept_at: counter, game_starts_at: counter + 15.minutes, game_ends_at: counter + 25.minutes)
      counter += 25.minutes
    end
  end

end
