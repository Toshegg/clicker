class TransactionType < ActiveRecord::Base
  include Concerns::Model

  has_many :user_transactions

  validates_uniqueness_of :name

end