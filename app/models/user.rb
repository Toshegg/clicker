class User < ActiveRecord::Base
  include Concerns::Model

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :user_clicks
  has_many :user_bets
  has_many :user_transactions

  def total_amount
    res = self.user_transactions.includes(:transaction_type).where('user_id = ?', self.id)
    total = 0
    res.each do |tr|
      if tr.transaction_type.name == 'refill' || tr.transaction_type.name == 'unhold'
        total += tr.amount
      else
        total -= tr.amount
      end
    end

    total
  end

  validates_presence_of :password_confirmation, :nickname
  validates_uniqueness_of :nickname
end
