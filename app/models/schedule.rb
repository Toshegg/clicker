class Schedule < ActiveRecord::Base
  include Concerns::Model

  has_many :user_clicks
  has_many :user_bets

  def self.get_current_schedule
    time_now = DateTime.now
    where('(bets_accept_at < ? AND game_starts_at > ? AND game_ends_at > ?) OR (bets_accept_at < ? AND game_starts_at < ? AND game_ends_at > ?)', time_now, time_now, time_now, time_now, time_now, time_now).first
  end

end