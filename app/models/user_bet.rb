class UserBet < ActiveRecord::Base
  include Concerns::Model

  belongs_to :user
  belongs_to :schedule
  belongs_to :user_transaction
  belongs_to :button_type

end