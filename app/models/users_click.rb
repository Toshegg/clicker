class UsersClick < ActiveRecord::Base
  include Concerns::Model

  belongs_to :user
  belongs_to :schedule
  belongs_to :button_type

end