class ButtonType < ActiveRecord::Base
  include Concerns::Model

  has_many :user_clicks
  has_many :user_bets

  validates_uniqueness_of :name
end