class UserTransaction < ActiveRecord::Base
  include Concerns::Model

  belongs_to :user
  belongs_to :transaction_type
  has_many :user_bets

  validates_presence_of :amount

  validate do |transaction|
    if User.find(transaction.user_id).total_amount < transaction.amount && (transaction.transaction_type.name == 'withdrawal' || transaction.transaction_type.name == 'hold')
      errors.add(:amount, I18n.t("insufficient_funds"))
    end

    time = DateTime.now
    if time > Schedule.get_current_schedule.game_starts_at && transaction.transaction_type.name == 'hold'
      errors.add(:amount, I18n.t("cant_bet_during_play"))
    end
  end
end