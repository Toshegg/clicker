module Concerns
  module Model
    extend ActiveSupport::Concern

    included do
      Rails.logger.debug self.table_name
      self.table_name = ['clicker', self.table_name].join('.')
    end
  end
end