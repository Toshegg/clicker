unless @user.nil?
  json.user do
    json.partial! 'users/object', user: @user
  end
end