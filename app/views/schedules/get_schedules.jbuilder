json.schedules do
  json.array! @schedules, partial: 'schedules/object', as: :schedule
end