json.user_transaction do
  json.partial! 'user_transactions/object', user_transaction: @user_transaction
end