json.users_click do
  if @click.present?
    json.button_type @click.button_type.name
  else
    json.button_type nil
  end
end