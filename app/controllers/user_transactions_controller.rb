class UserTransactionsController < ApplicationController
  before_action :authenticate_user!

  def create
    type = TransactionType.find_by(name: user_transactions_params[:transaction_type])
    @user_transaction = UserTransaction.create(user_id: current_user.id, amount: user_transactions_params[:amount], transaction_type_id: type.id)

    if type.name == 'hold' && !@user_transaction.id.nil?
      type = ButtonType.find_by(name: user_transactions_params[:button_type])
      schedule = Schedule.get_current_schedule
      UserBet.create(schedule_id: schedule.id, user_transaction_id: @user_transaction.id, button_type_id: type.id, user_id: current_user.id)

    end

    respond_with @user_transaction
  end

  private

  def user_transactions_params
    params.require(:user_transaction).permit(:transaction_type, :amount, :button_type)
  end
end