class SchedulesController < ApplicationController

  def get_schedules
    time_now = DateTime.now
    @schedules = Schedule.where('(bets_accept_at < ? AND game_starts_at > ? AND game_ends_at > ?) OR (bets_accept_at < ? AND game_starts_at < ? AND game_ends_at > ?) OR (bets_accept_at > ? AND game_starts_at > ? AND game_ends_at > ?) AND game_ends_at < ?', time_now, time_now, time_now, time_now, time_now, time_now, time_now, time_now, time_now, time_now + 2.hours)
    respond_with @schedules
  end
end