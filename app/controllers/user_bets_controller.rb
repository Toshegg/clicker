class UserBetsController < ApplicationController
  def get_all_bets
    schedule = Schedule.get_current_schedule
    bets = UserBet.includes(:user_transaction).where(schedule_id: schedule.id)
    @green_amount = 0
    @red_amount = 0

    bets.joins(:button_type).where('button_types.name = ?', 'green').each {|res| @green_amount += res.user_transaction.amount }
    bets.joins(:button_type).where('button_types.name = ?', 'red').each {|res| @red_amount += res.user_transaction.amount }
    respond_with @green_amount
  end
end