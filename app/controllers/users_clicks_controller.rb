class UsersClicksController < ApplicationController
  before_action :authenticate_user!, except: [:get_current_user_click, :get_button_clicks]

  def get_button_clicks
    schedule = Schedule.get_current_schedule
    @clicks = UsersClick.where(schedule_id: schedule.id, clicked: true)

    @red_clicks = @green_clicks = 0

    @red_clicks = @clicks.joins(:button_type).where('button_types.name = ?', 'red').length if @clicks.present?
    @green_clicks = @clicks.joins(:button_type).where('button_types.name = ?', 'green').length if @clicks.present?

    respond_with @clicks
  end

  def create
    schedule = Schedule.get_current_schedule
    button_type = ButtonType.find_by(name: user_clicks_params[:button_type])

    @click = UsersClick.where(schedule_id: schedule.id, user_id: current_user.id)

    if @click.present?

      @click = @click.where('clicked = ?', true)
      if @click.present? && @click.first.button_type_id != button_type.id
        # @click.first.errors[:base] << 'wrong click'
        # TODO: error message
        raise StandardError, 'qweqwe'
      elsif !@click.present?
        @click = UsersClick.create(schedule_id: schedule.id, user_id: current_user.id, button_type_id: button_type.id, clicked: true)
      elsif @click.present? && @click.first.button_type_id == button_type.id
        @click = @click.first.update(clicked: false)
      end
    else

      @click = UsersClick.create(schedule_id: schedule.id, user_id: current_user.id, button_type_id: button_type.id, clicked: true)
    end

    respond_with @click
  end

  def get_current_user_click
    @click = {}
    if current_user.present?
      schedule = Schedule.get_current_schedule

      @click = UsersClick.find_by(schedule_id: schedule.id, clicked: true, user_id: current_user.id)
    end

    respond_with @click

  end

  private

  def user_clicks_params
    params.require(:users_click).permit(:button_type)
  end
end