angular
.module('clicker')
  .directive 'serverError', ->
    {
    restrict: 'A'
    require: '?ngModel'
    link: (scope, element, attrs, ctrl) ->
      element.on 'input', ->
        scope.$apply ->
          ctrl.$setValidity('server', true)

          if attrs?.name && scope?.errors && scope?.errors[attrs.name]
            scope?.errors[attrs.name] = null
    }