angular
.module('clicker')
.controller 'PersonalAreaTransactionsCtl', ($scope, $state, UserTransaction) ->
  $scope.click = (transaction_type) ->
    if transaction_type == 'refill'
      $scope.tr = UserTransaction.create({transaction_type: transaction_type, amount: $scope.refill_amount}, (response) ->
        console.log response
      )
    else
      $scope.tr = UserTransaction.create({transaction_type: transaction_type, amount: $scope.withdrawal_amount}, (response) ->
        console.log response
      )