angular
.module('clicker')
.controller 'LoginCtl', ($scope, $state, Auth) ->

  $scope.login = () ->

    config = headers: 'X-HTTP-Method-Override': 'POST'

    Auth.login($scope.user, config).then ((user) ->
      $state.go('main.index')
      return
    ), (error) ->
      $scope.error = error.data.error
      return
    $scope.$on 'devise:login', (event, currentUser) ->
      # after a login, a hard refresh, a new tab
      return
    $scope.$on 'devise:new-session', (event, currentUser) ->
      # user logged in by Auth.login({...})
      return

  $scope.register = () ->
    $state.go('main.register')