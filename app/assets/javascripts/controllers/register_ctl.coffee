angular
.module('clicker')
.controller 'RegisterCtl', ($scope, $state, serverErrorsHandle, Auth) ->

  config = headers: 'X-HTTP-Method-Override': 'POST'

  $scope.errors = {}

  $scope.$on 'devise:new-registration', (event, user) ->
    return

  $scope.saveUser = () ->
    Auth.register($scope.user, config).then ((registeredUser) ->
      config = headers: 'X-HTTP-Method-Override': 'DELETE'
      $state.go('main.login')

      return
    ), (response) ->
      serverErrorsHandle(response, scope: $scope)
      return
