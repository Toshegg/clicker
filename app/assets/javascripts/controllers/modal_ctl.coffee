angular
.module('clicker')
.controller 'ModalCtl', ($scope, $state, $modalInstance, UserTransaction, type) ->
  $scope.bet = {}
  success = (response) ->
    $modalInstance.dismiss('ok')
  failure = (response) ->
    $scope.errors = response.data
  $scope.ok = () ->
    $scope.tr = UserTransaction.create({transaction_type: 'hold', amount: $scope.bet.amount, button_type: type}, success, failure)
  $scope.cancel = () ->
    $modalInstance.dismiss('cancel')