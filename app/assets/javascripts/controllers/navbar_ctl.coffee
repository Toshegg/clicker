angular
.module('clicker')
.controller 'NavbarCtl', ($scope, $state, $log, Auth, User) ->

  $scope.toggleDropdown = ($event) ->
    $event.preventDefault()
    $event.stopPropagation()
    $scope.status.isopen = !$scope.status.isopen
    return

  $scope.toggled = (open) ->
    return

  $scope.resolveState = (state) ->
    if state != 'logout'
      $state.go(state)
    else
      $scope.logout()


  $scope.userLoggedIn = (logged, user) ->
    $scope.user = user
    if (logged)
      $scope.items = [
        {
          name: 'Личный кабинет',
          state: 'main.personal_area.info'
        }
        {
          name: 'Выход',
          state: 'logout'
        }
      ]
    else
      $scope.items = [
        {
          name: 'Зарегистрироваться',
          state: 'main.register'
        }
        {
          name: 'Войти',
          state: 'main.login'
        }
      ]
    return

  $scope.logout = () ->
    config = headers: 'X-HTTP-Method-Override': 'DELETE'

    Auth.logout(config).then (() ->
      $scope.current_user = undefined
      return
    ), (error) ->
      return

  User.current().$promise.then (
    (user) ->
      if user.nickname
        $scope.userLoggedIn(true, user)
      else
        $scope.userLoggedIn(false, user)
  )

  $scope.$on 'devise:new-session', (event, currentUser) ->
    $scope.userLoggedIn(true, currentUser)
    return

  $scope.$on 'devise:logout', (event, oldCurrentUser) ->
    $scope.userLoggedIn(false, undefined)
    return

  $scope.$on 'devise:login', (event, currentUser) ->
    $scope.userLoggedIn(true, currentUser)
    return

  $scope.$on 'devise:new-registration', (event, user) ->
    $scope.userLoggedIn(true, user)
    return

  $scope.status = isopen: false


