angular
.module('clicker')
.controller 'IndexCtl', ($scope, $state, $interval, Auth, User, Schedule, $modal, UsersClick, UserBet) ->

  $scope.schedules = Schedule.get_schedules()
  $scope.green_toggled = $scope.red_toggled = false

  $scope.button_toggled = UsersClick.get_current_user_click((response) ->
    if response.button_type == 'green'
      $scope.green_toggled = true
    else if response.button_type == 'red'
      $scope.red_toggled = true
  )

  UsersClick.get_button_clicks((response) ->
    $scope.green_clicks = response.green_clicks
    $scope.red_clicks = response.red_clicks
  )

  UserBet.get_all_bets((response) ->
    $scope.green_amount = response.green_amount
    $scope.red_amount = response.red_amount
  )

  $interval(->
    UsersClick.get_button_clicks((response) ->
      $scope.green_clicks = response.green_clicks
      $scope.red_clicks = response.red_clicks
    )
    UserBet.get_all_bets((response) ->
      $scope.green_amount = response.green_amount
      $scope.red_amount = response.red_amount
    )
  ,5000)

  $scope.type = 'success'
  $scope.slider = {}

  $scope.flag = false

  millisToMinutesAndSeconds = (millis) ->
    minutes = Math.floor(millis / 60000)
    seconds = ((millis % 60000) / 1000).toFixed(0)
    if minutes > 0
      result = minutes + " мин "
    else
      result = " "
    if seconds < 10
      result += '0' + seconds + ' сек'
    else
      result += seconds + ' сек'
    return result


  $scope.schedules.$promise.then (schedules) ->
      $scope.interval = $interval(->
        date = new Date()
        current_schedule = schedules.filter((sch) -> new Date(sch.bets_accept_at) < date).slice().pop()

        if (current_schedule == undefined)
          $scope.schedules = Schedule.get_schedules()
          return

        bets_accept_at = new Date(current_schedule.bets_accept_at)
        game_starts_at = new Date(current_schedule.game_starts_at)
        game_ends_at = new Date(current_schedule.game_ends_at)

        if (date < game_starts_at)

          remained = millisToMinutesAndSeconds(game_starts_at - date)
          $scope.slider.remained = "До начала игры осталось " + remained
          $scope.slider.percents = Math.floor(100*(date - bets_accept_at)/(game_starts_at - bets_accept_at))

        else if (date > game_starts_at && date < game_ends_at)

          remained = millisToMinutesAndSeconds(game_ends_at - date)
          $scope.slider.remained = "До конца игры осталось " + remained
          $scope.slider.percents = Math.floor(100*(date - game_starts_at)/(game_ends_at - game_starts_at))

        if $scope.slider.percents < 85
          $scope.type = 'success'
        else if $scope.slider.percents < 95
          $scope.type = 'warning'
        else
          $scope.type = 'danger'

      , 1000)

  $scope.green_amount = $scope.red_amount = 0

  getRandomInt = (min, max) ->
    Math.floor(Math.random() * (max - min + 1)) + min

  $scope.button_clicked = (type) ->
    if type == 'green' && !$scope.green_toggled || type == 'red' && !$scope.red_toggled
      modalInstance = $modal.open(
        animation: true
        templateUrl: 'modal.html'
        controller: 'ModalCtl'
        size: 'lg'
        resolve: {
          type: () ->
            return type
        }
      )
    UsersClick.create({button_type: type})

  #slider

  $scope.myInterval = 5000
  slides = $scope.slides = []
  text = [
    'Определись какой цвет тебе нравится. Обычно выбирают синий :)',
    'Нажми на одну из кнопок и...',
    'Реши, готов ли ты рискнуть или просто зашёл поддержать друзей.',
    'Если решил играть, то пополни счёт и сделай ставку.',
    'Чтобы выиграть, команда должна наберать больше кликов по своей кнопке за 5 минут игры, чем команда соперников.',
    'Зови друзей, чтобы выиграть. Кто будет в команде, которая победит, тот удвоит свою ставку.'

  ]
  $scope.addSlide = (i) ->
    slides.push
      image: 'http://placekitten.com/850/400'
      text: text[i]
    return

  i = 0
  while i < 6
    $scope.addSlide(i)
    i++