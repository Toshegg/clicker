#= require jquery

#= require angular
#= require angular-ui-router
#= require angular-route
#= require angular-resource
#= require angular-rails-templates

#= require ui-bootstrap-0.13.0.min
#= require ui-bootstrap-tpls-0.9.0

#= require devise

#= require modules

#= require_tree ./services
#= require_tree ./constants
#= require_tree ./controllers
#= require_tree ./templates
#= require_tree ./directives

angular
.module 'clicker'
.config ($httpProvider, $stateProvider, $urlRouterProvider) ->

  #csrfToken = $("meta[name=\"csrf-token\"]").attr("content")

  $httpProvider.defaults.headers.common["Accept"]       = 'application/json'
  #$httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = csrfToken

  $urlRouterProvider.when('', '/index').when('/', '/index').otherwise('/index')

  $stateProvider
    .state 'main',
      abstract: true
      url: ''
      views:
        'navbar@':
          templateUrl: 'navbar.html'
          controller: 'NavbarCtl'

    .state 'main.index',
      url: '/index'
      views:
        'content@':
          templateUrl: 'index.html'
          controller: 'IndexCtl'
      onEnter: ($rootScope) ->
        $rootScope.title = 'Добро пожаловать!'


    .state 'main.register',
      url: '/register'
      views:
        'content@':
          templateUrl: 'register.html'
          controller: 'RegisterCtl'
      onEnter: ($rootScope) ->
        $rootScope.title = 'Регистрация'


    .state 'main.contacts',
      url: '/contacts'
      views:
        'content@':
          templateUrl: 'contacts.html'
          controller: 'ContactsCtl'


    .state 'main.login',
      url: '/login'
      views:
        'content@':
          templateUrl: 'login.html'
          controller: 'LoginCtl'
      onEnter: ($rootScope) ->
        $rootScope.title = 'Вход'

    .state 'main.info',
      url: '/info'
      views:
        'content@':
          templateUrl: 'info.html'
          controller: 'InfoCtl'
      onEnter: ($rootScope) ->
        $rootScope.title = 'F.A.Q.'

    .state 'main.personal_area',
      url: '/personal_area'
      abstract:true
      views:
        'content@':
          templateUrl: 'personal_area/_menu.html'

    .state 'main.personal_area.info',
      url: '/info'
      views:
        'content@main.personal_area':
          templateUrl: 'personal_area/personal_area_info.html'
          controller: 'PersonalAreaInfoCtl'
      onEnter: ($rootScope) ->
        $rootScope.title = 'Личный кабинет'

    .state 'main.personal_area.transactions',
      url: '/transactions'
      views:
        'content@main.personal_area':
          templateUrl: 'personal_area/personal_area_transactions.html'
          controller: 'PersonalAreaTransactionsCtl'
      onEnter: ($rootScope) ->
        $rootScope.title = 'Операции по счету'