angular
.module('clicker')
.factory 'UserBet', ($resource) ->
  $resource '/user_bets/:id.json', { id: '@id' },
    get_all_bets:
      url: '/get_all_bets'
      method:  'GET'
      isArray: false
      responseType: 'json'
      transformRequest: (data) ->
        angular.toJson(user_transaction: data)
      transformResponse: (data, headersGetter) ->
        if (typeof data == 'string')
          data = JSON.parse(data)
        data.errors || data.user_bet