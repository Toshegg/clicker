angular
.module('clicker')
.factory 'UsersClick', ($resource) ->
  $resource '/users_clicks/:id.json', { id: '@id' },
    create:
      method:  'POST'
      isArray: false
      responseType: 'json'
      transformRequest: (data) ->
        angular.toJson(users_click: data)
      transformResponse: (data, headersGetter) ->
        if (typeof data == 'string')
          data = JSON.parse(data)
        data.errors || data.users_click
    get_current_user_click:
      url: '/get_current_user_click'
      method:  'GET'
      isArray: false
      responseType: 'json'
      transformResponse: (data, headersGetter) ->
        if (typeof data == 'string')
          data = JSON.parse(data)
        data.errors || data.users_click

    get_button_clicks:
      url: '/get_button_clicks'
      method:  'GET'
      isArray: false
      responseType: 'json'
      transformResponse: (data, headersGetter) ->
        if (typeof data == 'string')
          data = JSON.parse(data)
        data.errors || data.users_clicks