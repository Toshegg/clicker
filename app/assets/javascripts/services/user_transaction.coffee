angular
.module('clicker')
.factory 'UserTransaction', ($resource) ->
  $resource '/user_transactions/:id.json', { id: '@id' },
    create:
      method:  'POST'
      isArray: false
      responseType: 'json'
      transformRequest: (data) ->
        angular.toJson(user_transaction: data)
      transformResponse: (data, headersGetter) ->
        if (typeof data == 'string')
          data = JSON.parse(data)
        data.errors || data.user_transaction