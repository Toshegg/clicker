angular
  .module 'clicker'
  .factory 'serverErrorsHandle', ->
    (response, options = {}) ->
      if options.scope
        form   = options.scope.form
        errors = options.scope.errors

      form   = options.form   if options.form
      errors = options.errors if options.errors

      angular.forEach errors, (value, id) ->
        unless form[id] == undefined
          form[id].$setValidity('server', true)
        delete errors[id]

      if form == undefined or errors == undefined
        throw "validationErrors expect `form` and `errors` arguments, or scope contains this two properties"
      
      unless response instanceof Object
        throw "validationErrors expect response to be object"

      unless response.status == 422
        return

      nerr = (errors, prefix, object) ->
        if object == undefined
          object = {}
        if prefix == undefined
          prefix = ''
        angular.forEach errors, (err_object, field_name) ->
          full_field_name = prefix + field_name
          if angular.isArray(err_object)
            object[full_field_name] = err_object
          else
            nerr err_object, full_field_name + '.', object
          return
        object

      _errors = nerr(response.data.errors)

      angular.forEach _errors, (errorMessages, field) ->
        unless form[field] == undefined
          form[field].$setValidity('server', false)

        errors[field] = errorMessages.join(', ')