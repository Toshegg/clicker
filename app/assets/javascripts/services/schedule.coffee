angular
.module('clicker')
.factory 'Schedule', ($resource) ->
  $resource '/schedules/:id.json', { id: '@id' },
    get_schedules:
      url: '/get_schedules'
      method:  'GET'
      isArray: true
      responseType: 'json'
      transformResponse: (data, headersGetter) ->
        if (typeof data == 'string')
          data = JSON.parse(data)
        data?.schedules