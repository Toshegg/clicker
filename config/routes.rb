Rails.application.routes.draw do
  devise_for :users, :controllers => {:registrations => "registrations"}
  resources :users
  resources :user_transactions, only: [:create]
  resources :users_clicks, only: [:create]

  get '/current_user', to: 'users#current'
  get '/get_schedules', to: 'schedules#get_schedules'
  get '/get_button_clicks', to: 'users_clicks#get_button_clicks'
  get '/get_current_user_click', to: 'users_clicks#get_current_user_click'
  get '/get_all_bets', to: 'user_bets#get_all_bets'



  root 'welcome#index'
end
