class CreateUserBets < ActiveRecord::Migration
  def change
    create_table 'clicker.user_bets' do |t|
      t.integer :user_id
      t.integer :schedule_id
      t.integer :user_transaction_id
      t.integer :button_type_id
    end
  end
end
