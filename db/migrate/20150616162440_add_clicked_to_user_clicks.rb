class AddClickedToUserClicks < ActiveRecord::Migration
  def change
    add_column 'clicker.users_clicks', :clicked, :boolean
  end
end
