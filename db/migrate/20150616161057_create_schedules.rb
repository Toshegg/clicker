class CreateSchedules < ActiveRecord::Migration
  def change
    create_table 'clicker.schedules' do |t|
      t.timestamp :bets_accept_at
      t.timestamp :game_starts_at
      t.timestamp :game_ends_at
    end
  end
end
