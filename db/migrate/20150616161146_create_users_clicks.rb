class CreateUsersClicks < ActiveRecord::Migration
  def change
    create_table 'clicker.users_clicks' do |t|
      t.integer :schedule_id
      t.integer :user_id
      t.integer :button_type_id
    end
  end
end
