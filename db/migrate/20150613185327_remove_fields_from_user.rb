class RemoveFieldsFromUser < ActiveRecord::Migration
  def change
    remove_column 'clicker.users', :url
    remove_column 'clicker.users', :username
    remove_column 'clicker.users', :provider
  end
end
