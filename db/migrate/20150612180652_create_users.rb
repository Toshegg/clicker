class CreateUsers < ActiveRecord::Migration
  def change
    create_table 'clicker.users' do |t|
      t.string :username
      t.string :nickname
      t.string :provider
      t.string :url

      t.timestamps null: false
    end
  end
end
