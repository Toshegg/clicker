class CreateUserTransactions < ActiveRecord::Migration
  def change
    create_table 'clicker.user_transactions' do |t|
      t.integer :user_id
      t.integer :transaction_type_id
      t.float :amount
    end
  end
end
