class CreateTransactionTypes < ActiveRecord::Migration
  def change
    create_table 'clicker.transaction_types' do |t|
      t.string :name
    end
  end
end
