%w(green red).each do |type|
  ButtonType.create(name:type)
end

%w(refill withdrawal hold unhold lost).each do |type|
  TransactionType.create(name: type)
end